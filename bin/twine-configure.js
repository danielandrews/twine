const program = require('commander')
const packageJSON = require('../package.json')
const configure = require('../commands/configure')

program
  .version(packageJSON.version)

program
  .command('consumer')
  .description('Add a Twitter API key and secret')
  .action(async () => {
    await configure.consumer(packageJSON.name)
  })

program.parse(process.argv)

if (!process.argv.slice(2).length) {
  program.outputHelp()
}
