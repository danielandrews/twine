const inquirer = require('inquirer')
const CredentialManager = require('../lib/CredentialManager')
const util = require('../lib/Utils')

const configure = {
  async consumer(name) {
    const credentials = new CredentialManager(name)
    const answers = await inquirer.prompt([
      {
        type: 'input',
        name: 'key',
        message: 'Enter your Twitter API Key',
        validate: util.notEmpty,
      },
      {
        type: 'password',
        name: 'secret',
        message: 'Enter your Twitter API Secret',
        validate: util.notEmpty,
      },
    ])
    await credentials.storeKeyAndSecret(answers.key, answers.secret)
  },
}

module.exports = configure
