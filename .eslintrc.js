module.exports = {
    "extends": "airbnb-base",
    "rules": {
        "semi": 0
    },
    "env": {
        "node": true,
        "jest": true
    }
};