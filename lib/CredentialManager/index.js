const ConfigStore = require('configstore')
const keytar = require('keytar')

class CredentialManager {
  constructor(name) {
    this.config = new ConfigStore(name)
    this.service = name
  }

  async getKeyAndSecret() {
    const key = this.config.get('apiKey')

    if (!key) {
      throw new Error('API key not found')
    }

    const secret = await keytar.getPassword(this.service, key)
    return [key, secret]
  }

  async storeKeyAndSecret(key, secret) {
    this.config.set('apiKey', key)
    await keytar.setPassword(this.service, key, secret)
  }
}

module.exports = CredentialManager
