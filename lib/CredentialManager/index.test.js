const fs = require('fs')
const path = require('path')

const keytar = require('keytar')
const CredentialManager = require('./')

jest.mock('keytar');

describe('CredentialManager', () => {
  let credentialManager
  let key
  let secret

  beforeEach(() => {
    credentialManager = new CredentialManager('twine-test')
    key = 'dummyKey'
    secret = 'dummySecret'
    const dummyCredentials = { key, secret }
    keytar.getPassword.mockResolvedValue(dummyCredentials.secret);
  })

  describe('getKeyAndSecret', () => {
    it('should get the twitter api key and secret', async () => {
      const existing = 'Existing'
      credentialManager.config.set('apiKey', key + existing)
      keytar.getPassword.mockResolvedValue(secret + existing);

      await expect(credentialManager.getKeyAndSecret())
        .resolves.toEqual([key + existing, secret + existing]);
    })

    it('should throw an error if there is no key', async () => {
      credentialManager.config.set('apiKey', null)

      expect.assertions(1);
      await expect(credentialManager.getKeyAndSecret()).rejects.toEqual(new Error('API key not found'));
    })
  })

  describe('storeKeyAndSecret', () => {
    it('should store the twitter api key and secret', async () => {
      const spy = jest.spyOn(credentialManager.config, 'set');

      await credentialManager.storeKeyAndSecret(key, secret)

      expect(keytar.setPassword).toBeCalledWith(credentialManager.service, key, secret);
      expect(spy).toBeCalledWith('apiKey', key);
    })
  })

  afterEach((done) => {
    fs.unlink(path.join(process.env.HOME, '.config', 'configstore', 'twine-test.json'), done)
  })
})
